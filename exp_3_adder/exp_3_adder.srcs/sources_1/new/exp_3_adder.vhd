----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.03.2024 11:01:53
-- Design Name: 
-- Module Name: exp_3_adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity exp_3_adder is
  Port ( 
    a: in std_logic;
    b: in std_logic;
    carry_in: in std_logic;
    s_half: in std_logic;
    sum: out std_logic;
    carry: out std_logic;
    led_a: out std_logic;
    led_b: out std_logic;
    led_c: out std_logic;
    led_s_half: out std_logic
  );
end exp_3_adder;

architecture Behavioral of exp_3_adder is

begin

        adder_process: process(a,b,carry_in,s_half)
        begin
            if s_half = '1' then
                carry <= a and b;
                sum <= a xor b;
             else   
                sum <= a xor b xor carry_in;
                carry <= ((a xor b) and carry_in) or (a and b);
             end if;
        end process adder_process;
        
        led: process(a,b,carry_in,s_half)
        begin
            led_a <= a;
            led_b <= b;
            led_s_half <= s_half;
            if s_half = '0' then
                led_c <= carry_in;
            else
                led_c <= '0';
             end if;
        end process led;

end Behavioral;
