----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.03.2024 09:23:56
-- Design Name: 
-- Module Name: exp_2_logic - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity exp_2_logic is
    Port ( 
     a: in std_logic;
     b: in std_logic;
     s_and: in std_logic;
     s_nand: in std_logic;
     s_or: in std_logic;
     s_nor: in std_logic;
     s_xor: in std_logic;
     s_xnor: in std_logic;
     led: out std_logic_vector(0 to 6);
     sonuc: out std_logic);
end exp_2_logic;

architecture Behavioral of exp_2_logic is          
begin
    led(0) <= a;
    led(1) <= b;
    led(2) <= s_and;
    led(3) <= s_nand;
    led(4) <= s_or;
    led(5) <= s_nor;
    led(6) <= s_xor;
    
    
    logic_process: process(a,b,s_and,s_nand,s_or,s_nor,s_xor,s_xnor)
      begin
        if s_and = '1' then
            sonuc <= a and b;
        elsif s_nand = '1' then
            sonuc <= a nand b;
        elsif s_or = '1' then
            sonuc <= a or b;
        elsif s_nor = '1' then
            sonuc <= a nor b;
        elsif s_xor = '1' then
            sonuc <= a xor b;
        elsif s_xor = '1' then
            sonuc <= a xnor b;
        end if;
      end process logic_process;

end Behavioral;
